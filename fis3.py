# SublimeText3 Plugin Doc:http://www.sublimetext.com/docs/3/api_reference.html
# python doc: http://www.runoob.com/python/python-tutorial.html
# OS doc: https://docs.python.org/3/library/os.html#os.system
# 给右键菜单提供一个命令，点击弹出 cmd 并切换目录到 fis 工程根目录

import os, sublime_plugin
from . import util # https://github.com/odyssomay/sublime-lispindent/issues/25

class Fis3Command(sublime_plugin.WindowCommand):
    def run(self):
        file_name = self.window.active_view().file_name()
        path = file_name.split("\\")
        current_directory = util.getFisProjectPath( path )
        commands = [
            "fis3 release -r"
        ]
        util.runCommand( current_directory, commands)
        return;