# sublimetext3-plugin
> sublimetext3 之 FIS 周边增强工具

# 功能描述
## 右键菜单增强功能
- CMD: 以当前文件所在目录打开系统 CMD 窗口
- goToFisProjectRoot: 打开 CMD 窗口，目录为当前文件所在的 fis3 工程根目录
- fis3-release: 在当前文件所在的 fis3 工程中执行`fis3 release -r`

## 改装`Ctrl + P`
- go_to_file.py: 自动填充已选文本到`Ctrl + P`面板

# 安装
- 打开 `Preferences -> Browser Packages...`目录
- `git clone https://git.oschina.net/g8up/sublimetext3-plugin.git`