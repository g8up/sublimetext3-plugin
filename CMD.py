# coding=utf-8

import os, sublime_plugin, sublime

class CmdCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        '打开 CMD，目录切换到当前文件目录'

        file_name=self.view.file_name()
        path = file_name.split("\\")
        current_driver = path[0]
        path.pop()
        current_directory = "\\".join(path)
        command = "cd " + current_directory + " & " + current_driver + " & start cmd"
        os.system(command)