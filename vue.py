# coding=utf-8

# 将当前目录的 Vue 组件分散的 html/es(js)/less 合并成单文件(.vue)
# 依赖这个 vw 命令行工具：http://git.oschina.net/g8up/Vue-widget
# 用法：在打开的某一分散文件（html/es(js)/less）中右键菜单中触发

import os, sublime_plugin, sublime
from . import util

class VueCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        filePath = self.view.file_name()
        currentDirectory = util.getFileCurrentPath( filePath )
        commands = [
            "vw -c"
        ]
        util.runCommand( currentDirectory, commands)