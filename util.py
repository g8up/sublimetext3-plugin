# @file util.py
# @desc 置于 Preferences -> Browser Packages 弹出的目录下

import os, sublime

def runCommand( cd, commands ):
    '解析命令行'

    if( len( commands ) > 0 ):
        if( cd != "" ):
            os.chdir( cd )

        commands.reverse()
        commandStr = " & ".join( commands )
        print( "执行命令", commandStr )
        os.system( commandStr )
    else:
        sublime.message_dialog( "命令不能为空" )

def getFisProjectPath( paths ):
    '解析路径到 fis 工程根目录'

    while( len (paths) > 0 and not os.path.exists( "\\".join( paths ) + "\\widget" ) ):
        curPath = paths.pop()
    # sublime.message_dialog( str( len (paths))  )
    if( len (paths) == 0 ):
        return ""
    return "\\".join(paths)

def getFileCurrentPath( filePath ):
    path=filePath.split("\\")
    current_driver=path[0]
    path.pop()
    return "\\".join(path)