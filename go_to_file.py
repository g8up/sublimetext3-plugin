# SublimeText3 增强：自动填充已选文本到`Ctrl + P`面板
# Repo: https://gist.github.com/g8up/405c9372512be540cd6bfe13a49ce6e1

import sublime_plugin

class ShowGotoOverlayWithSelectionCommand(sublime_plugin.WindowCommand):
    def run(self):
        window = self.window
        view = window.active_view()
        text = view.substr(view.sel()[0])
        window.run_command("show_overlay", {
            "overlay": "goto",
            "show_files": True,
            "text": text
        })