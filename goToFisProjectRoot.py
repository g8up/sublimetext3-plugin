# @file goToProjectRoot.py
# SublimeText3 Plugin Doc:http://www.sublimetext.com/docs/3/api_reference.html
# python doc: http://www.runoob.com/python/python-tutorial.html
# 给右键菜单提供一个命令，点击弹出 cmd 并切换目录到 fis 工程根目录
import os, sublime_plugin, sublime
from . import util

class GoToFisProjectRootCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        file_name = self.view.file_name()
        # sublime.message_dialog( file_name ) # debug
        path = file_name.split("\\")
        current_directory = util.getFisProjectPath( path )
        commands = [
            "start cmd"
        ]
        util.runCommand( current_directory, commands)